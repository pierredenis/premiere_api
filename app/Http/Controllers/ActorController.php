<?php

namespace App\Http\Controllers;

use App\Http\Resources\ActorCollection;
use Illuminate\Http\Request;
use App\Models\Actor;
use App\Http\Resources\ActorResource;
class ActorController extends Controller
{
    public function index(){
        return new ActorCollection(Actor::paginate());
    }

    public function show($id){
        return new ActorResource(Actor::find($id));
    }
}

