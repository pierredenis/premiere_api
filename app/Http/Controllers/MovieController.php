<?php

namespace App\Http\Controllers;

use App\Http\Resources\MovieCollection;
use App\Http\Resources\MovieResource;
use Illuminate\Http\Request;
use App\Models\Movie;

class MovieController extends Controller
{
    public function index(){
        return new MovieCollection(Movie::all());
    }
    public function show($id){
        return new MovieResource(Movie::find($id));
    }
}
